## [0.0.1] - 2020-08-11

* Initial release: A date picker styled dropdown widget.

## [0.0.2] - 2020-08-11

* Added example (failed)
* getDate() function adds a leading zero to month/day if length equals 1

## [0.0.3] - 2020-08-11

* Added example