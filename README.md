Easy to use flutter package to get and set dates. The package builds DropdownButton widgets to set dates (year, month, day).

## ValidDate and NullableValidDate immutable data classes:

These classes are for date validation. NullableValidDate can have null parameters but ValidDate can't. In case of invalid date it will assert an error to makes invalid dates unrepresentable.

Both ValidDate and NullableValidDate is derived from Date class.

## DropdownDatePicker widget parameters:

To instantiate the widget use DropdownDatePicker constructor.

### Required parameters:

**firstDate:** Provide a minimum date.

**lastDate:** Provide a maximum date.

*Note:* firstDate must be smaller than lastDate.

### Optional parameters:

**initialDate:** Null by default. If initialDate is not provided a default hintText will be shown in their DropdownButton widgets. 

*Note:* By passing an initialDate make sure if it is a valid date and it must be between the range of first and last date. Else it will result an assertion error.

**dateFormat:** By default it gets a value of DateFormat.ymd. Use DateFormat.mdy or DateFormat.dmy to change.

**textStyle:** Overrides default TextStyle of DropdownButton.

**underLine:** Replaces default underLine Widget of DropdownButton.

## Examples:

```dart
final now = DateTime.now();

DropdownDatePicker(
  firstDate: ValidDate(year: now.year-100, month: 01, day: 01),
  lastDate: ValidDate(year: now.year, month: now.month, day: now.month),
),
```
The above example will create a DropdownDatePicker instance with only the required parameters.

```dart
final datePicker = DropdownDatePicker(
  firstDate: ValidDate(year: 2005, month: 01, day: 01),
  lastDate: ValidDate(year: 2020, month: 12, day: 12),
  initialDate: NullableDate(year: 2010, month: null, day: 24),
  dateFormat: DateFormat.dmy
),
```
If you'd like to override default initialDate and dateFormat.

Retrieve date values like this:

```dart
datePickerInstance
  ..currentDate
  ..firstDate
  ..lastDate
  ..dateFormat
  ..year
  ..month
  ..day
  ..getDate('/');
```

## DateUtil utility functions:

* isLeapYear(year): bool
* getMaxDayByDate(month, year): int
* isValidDate(year, month, day): bool
* isNullableValidDate(year, month, day): bool

## Versions:

* 0.0.1

