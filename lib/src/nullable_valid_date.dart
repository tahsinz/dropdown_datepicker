part of dropdown_date_picker;

/// Valid nullable date class
@immutable
class NullableValidDate extends Date {
  /// Returns a [NullableValidDate] object if parameters are valid
  /// otherwise asserts an error.
  NullableValidDate({
    final int year,
    final int month,
    final int day,
  })  : assert(DateUtil.isNullableValidDate(
            year: year,
            month: month,
            day: DateUtil.fixDay(year: year, month: month, day: day))),
        super(
          year: year,
          month: month,
          day: DateUtil.fixDay(year: year, month: month, day: day),
        );
}
